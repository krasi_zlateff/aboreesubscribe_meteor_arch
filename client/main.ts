// // DEVELOPMENT
// import './imports/polyfills';
// import { Meteor } from 'meteor/meteor';
//
// import { enableProdMode } from '@angular/core';
// import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
// import { AboreeModule as aboree } from './imports/aboree/core.module';
//
// Meteor.startup(() => {
//
//   if (Meteor.isProduction) {
//     enableProdMode();
//   }
//
//   platformBrowserDynamic().bootstrapModule(aboree).catch(err => console.log(err));
// });

// PRODUCTION
import './imports/polyfills';
import { Meteor } from 'meteor/meteor';

import { enableProdMode } from '@angular/core';
import { platformBrowser } from '@angular/platform-browser';
import { AboreeModuleNgFactory } from './imports/aboree/core.module.ngfactory';

Meteor.startup(() => {

  if (Meteor.isProduction) {
    enableProdMode();
  }

  platformBrowser().bootstrapModuleFactory(AboreeModuleNgFactory);
});
