import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from "../../../../../../../imports/models";

@Component({
  selector: 'subscribe-form',
  templateUrl: './subscribe-form.component.html',
  styleUrls: [ './subscribe-form.component.scss' ]
})
export class SubscribeFormComponent implements OnInit {
  @Output() submit = new EventEmitter<User>();

  subscribeForm = new FormGroup({
    email: new FormControl('', [ Validators.required, Validators.email ]),
  });

  constructor() {
  }

  ngOnInit() {
  }

  onEnter(event: any) {
    if(event.keyCode == 13) {
      // alert('you just clicked enter');
      this.subscribe();
    }
  }

  subscribe() {
    if (this.subscribeForm.value.email) {
      this.submit.emit({
        email: this.subscribeForm.value.email
      });
    }
  }
}
