import { Component, OnInit } from '@angular/core';
import { User } from "../../../../../../../imports/models";
import { AlertDialogComponent } from "../alert-dialog/alert-dialog.component";
import { MatDialog } from "@angular/material";
import { MeteorObservable } from "meteor-rxjs";

@Component({
  selector: 'subscribe-dialog',
  templateUrl: './subscribe-dialog.component.html',
  styleUrls: [ './subscribe-dialog.component.scss' ]
})
export class SubscribeDialogComponent implements OnInit {

  subscribed: boolean = false;

  constructor(public dialog: MatDialog) {
  }

  ngOnInit() {
  }

  subscribe(subscribe: User) {

    if (subscribe.email === undefined) {
      return
    } else {
      MeteorObservable.call('subscribeUser', subscribe.email).subscribe({
        next: () => {
          this.subscribed = true;
        },
        error: (e: Error) => {
          this.dialog.open(AlertDialogComponent);
        }
      })
    }
  }
}
