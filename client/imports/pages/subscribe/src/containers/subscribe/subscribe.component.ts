import { Component, OnInit } from '@angular/core';
import { MatDialog } from "@angular/material";
import { SubscribeDialogComponent } from "../subscribe-dialog/subscribe-dialog.component";

@Component({
  selector: 'subscribe',
  templateUrl: './subscribe.component.html',
  styleUrls: [ './subscribe.component.scss' ]
})
export class SubscribeComponent implements OnInit {

  constructor(public dialog: MatDialog) {
  }

  ngOnInit() {
  }

  openDialog(): void {
    this.dialog.open(SubscribeDialogComponent, {
      width: '300px'
    });
  }
}
