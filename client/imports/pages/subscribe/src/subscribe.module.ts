import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { SubscribeComponent } from "./containers/subscribe/subscribe.component";
import { SubscribeFormComponent } from "./components/login-form/subscribe-form.component";
import { SubscribeDialogComponent } from "./containers/subscribe-dialog/subscribe-dialog.component";
import { AlertDialogComponent } from "./containers/alert-dialog/alert-dialog.component";
import { MaterialModule } from "../../../libs/material";

const COMPONENTS = [ SubscribeComponent, SubscribeFormComponent, SubscribeDialogComponent, AlertDialogComponent ];

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MaterialModule,
    ReactiveFormsModule,
  ],
  declarations: [ COMPONENTS ],
  entryComponents: [
    SubscribeDialogComponent,
    AlertDialogComponent ],
  exports: [ COMPONENTS ]
})

export class SubscribeModule {

}
