import { NgModule } from '@angular/core';
import { RouterModule } from "@angular/router";
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { aboree } from './core.component';
import { LayoutModule } from "../libs/layout";
import { SubscribeComponent } from "../pages/subscribe/src/containers/subscribe/subscribe.component";
import { SubscribeModule } from "../pages/subscribe";

@NgModule({
  imports: [
    LayoutModule,
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(
      [
        { path: '', pathMatch: 'full', redirectTo: 'welcome' },
        { path: 'welcome', component: SubscribeComponent },
      ],
      { initialNavigation: 'enabled' }
    ),
    SubscribeModule
  ],
  declarations: [
    aboree,
  ],
  bootstrap: [ aboree ]
})
export class AboreeModule {
}
