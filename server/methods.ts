import { Meteor } from 'meteor/meteor';
import { Users } from "../imports/collections/users";

declare let SSR: any;

Meteor.methods({

  subscribeUser(email: string) {
    if (!email) {
      throw new Meteor.Error('enter email',
        'no email address');
    }

    let user = Users.find({ email: email }).cursor.count();

    if (user) {
      throw new Meteor.Error('registered email',
        'This email address is already registered.');
    }

    Users.insert({
      email: email
    });

    SSR.compileTemplate('htmlEmail', Assets.getText('html-email.html'));

    let emailData = {
      email: email
    };
    Email.send({
      to: email,
      from: "no-reply@aboree.com",
      subject: "aboree.com - Thank you for your interest",
      html: SSR.render('htmlEmail', emailData),
    });
  }
});
